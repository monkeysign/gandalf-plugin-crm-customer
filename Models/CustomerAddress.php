<?php

namespace Plugins\CRM\Customer\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class CustomerAddress extends Eloquent {

	protected $table = 'customer_address';
	protected $fillable = array('id_customer', 'city', 'nation', 'zipcode', 'address', 'phone','state', 'type', 'selected');

	public static function saveOrUpdate($item) {
		if (!isset($item['id']) || !$item['id']) {
			return CustomerAddress::create($item);
		} else {
			CustomerAddress::where('id', '=', $item['id'])->update($item);
			return CustomerAddress::find($item['id']);
		}
	}

	public function customer() {
		return $this->belongsTo('Plugins\CRM\Customer\Models\Customer', 'id', 'id_customer');
	}

}
