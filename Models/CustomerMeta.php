<?php

namespace Plugins\CRM\Customer\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class CustomerMeta extends Eloquent {

    protected $dates = ['deleted_at'];
    protected $table = 'customer_meta';
    protected $fillable = array('id_customer', 'meta_key', 'value', 'iso');
    public $incrementing = false;

    public function customer() {
        return $this->belongsTo('Plugins\CRM\Customer\Models\Customer', 'id', 'id_customer');
    }

}
