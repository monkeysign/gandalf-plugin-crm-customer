<?php

namespace Plugins\CRM\Customer\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of User
 *
 * @author Riccardo
 */
class Customer extends Eloquent {
	use SoftDeletes;

	protected $dates = ['deleted_at'];
	protected $table = 'customer';
	protected $fillable = array('name', 'surname', 'email', 'password', 'status');

	public static function saveOrUpdate($item) {
		if (!isset($item['id']) || !$item['id']) {
			hooks()->do_action( CRM_CUSTOMER_SAVE, $item );
			return Customer::create($item);
		} else {
			Customer::where('id', '=', $item['id'])->update($item);
			hooks()->do_action( CRM_CUSTOMER_SAVE, $item );
            hooks()->do_action( CRM_CUSTOMER_POST_SAVE, $item );
            /*die("sono nel save controller di Customer");*/
			return Customer::find($item['id']);
		}
	}

	/**
	 * Prende il PostMeta del post secondo una determinata chiave, o restituisce l'intera collection
	 * @param null $key
	 * @param null $isoLingua
	 *
	 * @return $this|mixed
	 */
	public function meta( $key = null, $isoLingua = null ) {
		// Lingua di default
		if ( ! $isoLingua ) {
			$isoLingua = config( 'locale' );
		}
		// Se non setto una chiave restituisco tutti i meta
		if ( ! $key ) {
			return $this->hasMany( 'Plugins\CRM\Customer\Models\CustomerMeta', 'id_customer', 'id' )
			            ->where( 'iso', $isoLingua );
		}
		// se ho la chiave restituisco il valore di quel meta in quella lingua
		$val =  $this->hasMany( 'Plugins\CRM\Customer\Models\CustomerMeta', 'id_customer', 'id' )
		             ->where( 'meta_key', $key )
		             ->where( 'iso', $isoLingua )
		             ->first();

		if($val) return $val->value;

		else return false;
	}

	/**
	 * Indirizzo di default spedizione
	 * @return Eloquent|null|object|static
	 */
	public function shipping_address(){
		$val =  $this->hasMany('Plugins\CRM\Customer\Models\CustomerAddress', 'id_customer', 'id' )
			->where('type', 1)
			->where('selected', 1)
			->first();

		if($val) return $val;

		else return false;
	}

	/**
	 * Indirizzo di default fatturazione
	 * @return Eloquent|null|object|static
	 */
	public function billing_address(){
		$val =  $this->hasMany('Plugins\CRM\Customer\Models\CustomerAddress', 'id_customer', 'id' )
		            ->where('type', 2)
		            ->where('selected', 1)
		            ->first();

		if($val) return $val;

		else return false;
	}

}