<?php

namespace Plugins\CRM\Customer\Controllers;

use Modules\Backend\Classes\Controller;
use Plugins\CRM\Customer\Models\Customer as Customer;
use Plugins\CRM\Customer\Models\CustomerAddress;
use Plugins\CRM\Customer\Models\CustomerMeta as CustomerMeta;

class Customers extends Controller {

    /**
     * Lista di tutti i record
     */
    public function listAll() {
        return view()->render('crm-customer.customer.list', []);
    }

    /**
     * Genero il json per la lista
     * @return array
     * @throws \Exception
     */
    public function recordList() {
        $record = new Customer();
        $dataTable = hooks()->apply_filters(CRM_ADMIN_CUSTOMER_LIST, $record);

        return $dataTable->make();
    }

    /**
     * Azione di form insert/update
     *
     * @param null $id
     */
    public function form($id = null) {
        if (isset($id) && $id) {
            $param['record'] = Customer::find($id);
        } else {
            $param['record'] = new Customer();
        }

        return view()->render('crm-customer.customer.form', $param);
    }

    /**
     * Delete di un record e meta associati
     *
     * @param null $id
     *
     * @return array
     */
    public function delete($id = null) {
        $record = Customer::find($id);
        CustomerMeta::where('id_customer', $record->id)->delete();
        CustomerAddress::where('id_customer', $record->id)->delete();
        $record->delete();
        //usare forceDelete() solo se si vuole una cancellazione fisica
        $data = array('result' => true);

        return $data;
    }

    /**
     * Delete di un gruppo e meta associati
     * @return array
     */
    public function deleteGroup() {
        // $_POST['ids']
        $group = request()->get('ids');
        CustomerMeta::whereIn('id_customer', $group)->delete();
        Customer::whereIn('id', $group)->delete();
        $data = array('result' => true);

        return $data;
    }

    /**
     * Azione di salvataggio
     * @return array
     */
    public function save() {
        $item = request()->get('item');
        //$validator = new \Modules\Backend\Classes\ValidationUsers();
        $noCryptPassword = '';
        if (isset ($item['password']) && $item['password']) {
            $noCryptPassword = $item['password'];
        }


        try {
            $customer = Customer::where('email', trim(strtolower($item['email'])))->first();
            if ($customer && (!isset($item['id']) || !$item['id'])) {
                $param = [
                    'state' => false,
                    'mex' => 'La mail inserita è già presente nel database'
                ];
                return $param;
            }

            //($validator->validate( $item );
            if ($this->isPasswordEncrypted($item)) {
                $item['password'] = md5($item['password']);
            } else {
                $newPassword = $this->generatePassword(8);
                $item['password'] = md5($newPassword);
            }

            $record = Customer::saveOrUpdate($item);
            //save meta
            $this->saveCustomerMeta($record);
            //save address
            $this->saveAddress($record);

            $param = [
                'record' => $record,
                'state' => true,
                'mex' => 'Salvataggio Riuscito'
            ];

            if (!isset($item['id']) || !$item['id']) {
                $record->password = $noCryptPassword;
                hooks()->do_action(CRM_ADMIN_REGISTER_SEND_MAIL, $record);
            }

        } catch (\Ring\Exception\ValidationException $ex) {
            $param = [
                'state' => false,
                'mex' => $ex->getMessage()
            ];
            return $param;
        }

        return $param;
    }

    /**
     * Check password
     *
     * @param $item
     *
     * @return bool
     */
    public function isPasswordEncrypted($item) {
        if (isset ($item['password']) && $item['password']) {
            return true;
        }

        return false;
    }


    /**
     * Generate password
     *
     *
     *
     * @return String value generate password
     */
    function generatePassword($length = 8, $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') {
        return substr(str_shuffle($chars), 0, $length);
    }

    public function saveAddress(\Plugins\CRM\Customer\Models\Customer $record) {
        $shipping_address = request()->get('shipping_address');
        $shipping_address['id_customer'] = $record->id;

        $billing_address = request()->get('billing_address');
        $billing_address['id_customer'] = $record->id;

        $shipping_obj = CustomerAddress::saveOrUpdate($shipping_address);
        $billing_address = CustomerAddress::saveOrUpdate($billing_address);
    }

    /**
     * Salva i meta
     *
     * @param Customer $record
     */
    public function saveCustomerMeta(\Plugins\CRM\Customer\Models\Customer $record) {
        $items = request()->get('meta');

        if (request()->get('language') && request()->get('language') !== '') {
            $iso = request()->get('language');
        } else {
            $iso = config('locale');
        }

        // Salvo il meta
        foreach ($items as $key => $value) {
            // Cancello le tassonomie associate
            CustomerMeta::where('id_customer', '=', $record->id)->where('iso', $iso)->where('meta_key', $key)->delete();

            $metaArray['meta_key'] = $key;
            if (is_array($items[$key])) {
                $metaArray['value'] = serialize($value);
            } else {
                $metaArray['value'] = $value;
            }
            $metaArray['id_customer'] = $record->id;
            $metaArray['iso'] = $iso;
            CustomerMeta::create($metaArray);
        }
    }
}