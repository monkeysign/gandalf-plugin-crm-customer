<?php

const _P_CRM_CUSTOMER = "\\Plugins\\CRM\\Customer\\Controllers\\";
// Admin group
router()->group( [ 'middleware' => [ 'web' ] ], function () {
	//auth route
	router()->group( [ 'middleware' => [ 'auth' ], 'prefix' => 'admin/crm' ], function () {
		//customer
		router()->group( [ 'prefix' => 'customer' ], function () {
			router()->get('', [ 'as' => 'admin.crm.customer.base', 'uses' => _P_CRM_CUSTOMER . 'Customers@listAll' ]);

			router()->get( '/list', [ 'as' => 'admin.crm.customer.list', 'uses' => _P_CRM_CUSTOMER . 'Customers@listAll' ] );
			router()->post( '/list/recordList', [ 'as' => 'admin.crm.customer.list.recordList', 'uses' => _P_CRM_CUSTOMER . 'Customers@recordList' ] );

			router()->get('/add', [ 'as' => 'admin.crm.customer.add', 'uses' => _P_CRM_CUSTOMER . 'Customers@form' ]);
			router()->get('/{id}', [ 'as' => 'admin.crm.customer.update', 'uses' => _P_CRM_CUSTOMER . 'Customers@form' ]);
			router()->post('/save', [ 'as' => 'admin.crm.customer.save', 'uses' => _P_CRM_CUSTOMER . 'Customers@save' ]);
			router()->get('/delete/{id}',[ 'as' => 'admin.crm.customer.delete', 'uses' => _P_CRM_CUSTOMER . 'Customers@delete' ]);
			router()->post('/delete', [ 'as' => 'admin.crm.customer.deletegroup', 'uses' => _P_CRM_CUSTOMER . 'Customers@deleteGroup' ]);
		});
	});
});