<?php

const CRM_ADMIN_CUSTOMER_LIST        = 'CRM_ADMIN_CUSTOMER_LIST';
const CRM_ADMIN_CUSTOMER_LIST_FILTER = 'CRM_ADMIN_CUSTOMER_LIST_FILTER';
const CRM_ADMIN_CUSTOMER_LIST_COLUMN = 'CRM_ADMIN_CUSTOMER_LIST_COLUMN';
const CRM_ADMIN_CUSTOMER_FORM        = 'CRM_ADMIN_CUSTOMER_FORM';
const CRM_ADMIN_CUSTOMER_FORM_META   = 'CRM_ADMIN_CUSTOMER_FORM_META';
const CRM_CUSTOMER_SAVE              = 'CRM_CUSTOMER_SAVE';
const CRM_CUSTOMER_POST_SAVE         = 'CRM_CUSTOMER_POST_SAVE';


//Qui hooks

/**
 * Aggiungo il path delle views di questo plugin grazie al filtro "views_path" che si trova nel controller generale
 */
hooks()->add_filter( APP_VIEWS_PATH, function ( $views = [] ) {
	return array_merge( [ __DIR__ . '/views/' ], $views );
} );

/**
 * Aggiungo le voci al menu tramite una vista presente nel plugin
 */
hooks()->add_action( ADMIN_LEFT_MENU, function () {
    if(user_logged()->role <= 1) {
        view()->render('crm-customer.left', []);
    }
}, 11 );

/**
 * INCLUDE GLI HOOK PER LA LISTA BACKEND
 */
include __DIR__ . '/config/hook_list.php';


/**
 * Includo gli helper
 */
require __DIR__ . '/helpers.php';