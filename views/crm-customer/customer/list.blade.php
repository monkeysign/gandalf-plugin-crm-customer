@extends('layout')

@section('content')
    <!-- PAGE CONTENT WRAPPER -->
    <div id="page-wrapper">
        <div class="container-fluid">

        @include('crm-customer.customer.header')

        <!-- START WIDGETS -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <div class="table-responsive">
                            <table id="tabella" class="table m-t-30 table-hover contact-list">
                                <thead>
                                <tr>
                                    <?php hooks()->apply_filters(CRM_ADMIN_CUSTOMER_LIST_COLUMN,[]); ?>
                                </tr>
                                </thead>

                                <tfoot>
                                <tr>
	                                <?php hooks()->apply_filters(CRM_ADMIN_CUSTOMER_LIST_COLUMN,[]); ?>
                                </tr>
                                </tfoot>

                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h5><strong>Azioni di gruppo</strong></h5>
                    <a id="deleteAll" data-path="{{ path_for('admin.user.deletegroup') }}"
                       class="btn btn-sm btn-danger delete-item"><span class='fa fa-trash-o'></span> Cancella</a>
                </div>
            </div>
            <!-- END WIDGETS -->
        </div>
        <!-- END PAGE CONTENT WRAPPER -->
    </div>
@endsection

@section('style')
    <link href="{{asset('assets/plugins/bower_components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet"
          type="text/css"/>
@endsection

@section('scripts')
    <script src="{{asset('assets/plugins/bower_components/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/js/functions_custom.js')}}"></script>
    <script>
        $(document).ready(function () {
            var table = $('#tabella').DataTable({
                responsive: true,
                ordering: false,
                displayLength: 25,
                "order": 0,
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": "{{ path_for('admin.crm.customer.list.recordList') }}",
                    "type": "POST"
                },
            });
            //la trovi in function_custom.js
            tabellaInit(table);
        });
    </script>
@endsection