@extends('layout')

@section('content')

    <!-- PAGE CONTENT WRAPPER -->
    <div id="page-wrapper">
        <div class="container-fluid">

            @include('crm-customer.customer.header')

            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h2>
                            @if($record->id)
                                <strong>{{$record->name}} {{$record->surname}}</strong> <span class="badge badge-info">Scheda Cliente</span>
                            @else
                                Nuovo Cliente
                            @endif
                        </h2>
                        <hr/>
                        <form id="form-save" action="{{ path_for('admin.crm.customer.save') }}" method="POST"
                              enctype="multipart/form-data">
                            <input id="item_id_hidden" type="hidden"
                                   value='@if($record->id){{ $record->id }}@else{{0}}@endif' name='item[id]'>
                            <div class="form-body">
                                <div class="row">

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Nome *</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{$record->name}}" required
                                                       data-toggle="validator" type="text" name="item[name]"
                                                       id="name" class="form-control"
                                                       placeholder="Nome">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>


                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label title="Campo Obbligatorio"
                                                   class="control-label">Cognome*</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{$record->surname}}" required
                                                       data-toggle="validator" type="text" name="item[surname]"
                                                       id="surname" class="form-control"
                                                       placeholder="Cognome">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label title="Campo Obbligatorio"
                                                   class="control-label">Email*</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-email"></i>
                                                </div>
                                                <input value="{{$record->email}}" type="email" required
                                                       name="item[email]" id="email" class="form-control"
                                                       placeholder="Email">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Telefono Predefinito</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-mobile"></i>
                                                </div>
                                                <input value="{{$record->meta('phone')}}"
                                                       data-toggle="validator" type="text" name="meta[phone]"
                                                       id="meta_phone" class="form-control">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label
                                                    title="Campo Password per la registrazione"
                                                    class="control-label">Password <a style="color:#444; background: #e6e6e6;" class="btn btn-xs" id="generatePassword">Genera Password</a></label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-lock"></i>
                                                </div>
                                                <input type="password" name="item[password]" id="password"
                                                       class="form-control"
                                                       placeholder="Password">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>

                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label
                                                    title="Autorizza o meno questo cliente a connettersi"
                                                    class="control-label">Stato {{ $record->status }}</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-light-bulb"></i>
                                                </div>
                                                <select name="item[status]" class="form-control select-choose">
                                                    <option
                                                            @if ( $record->status == \Ring\Support\Enum\Status::DISABLED)
                                                            selected @endif
                                                            value="{{ \Ring\Support\Enum\Status::DISABLED  }}">
                                                        Disabilitato
                                                    </option>
                                                    <option
                                                            @if ( $record->status == \Ring\Support\Enum\Status::ENABLED  || $record->status!==0)
                                                            selected @endif
                                                            value="{{\Ring\Support\Enum\Status::ENABLED}}">Abilitato
                                                    </option>
                                                </select>
                                            </div>
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->

                                @include('crm-customer.customer.meta_customer')

                                @include('crm-customer.customer.address_customer')

                                {{ hooks()->do_action(CRM_ADMIN_CUSTOMER_FORM, $record) }}

                            </div>
                            <!--/row-->

                            <!--/span-->
                            <div class="form-actions">
                                <hr style="margin-top: 0px">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Salva
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('style')
    <link href="{{asset('assets/plugins/bower_components/toast-master/css/jquery.toast.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('assets/plugins/bower_components/custom-select/custom-select.css')}}" rel="stylesheet"
          type="text/css">
@endsection

@section('scripts')
    <script src="{{asset('assets/js/validator.js')}}"></script>
    <script src="{{asset('assets/js/form.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/toast-master/js/jquery.toast.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/blockUI/jquery.blockUI.js')}}"></script>

    <!-- Select Plugin JavaScript -->
    <script src="{{asset('assets/plugins/bower_components/custom-select/custom-select.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.js')}}"
            type="text/javascript"></script>

    <script>
        // Select Choosen
        jQuery(document).ready(function () {
            $(".select-choose").select2();
        });

        jQuery('#generatePassword').click(function() {
            var randomstring = Math.random().toString(36).slice(-8);
            jQuery('#password').val(randomstring);
        });
    </script>
@endsection