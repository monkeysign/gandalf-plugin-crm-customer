<li>
    <a href="#" class="waves-effect"><i
                class="zmdi zmdi-face zmdi-hc-fw fa-fw"></i> <span class="hide-menu"> Clienti <span
                    class="fa arrow"></span></span></a>
    <ul class="nav nav-second-level">
        <li><a href="{{path_for('admin.crm.customer.list')}}">Vai alla lista</a></li>
        <li><a href="{{path_for('admin.crm.customer.add')}}">Aggiungi Nuovo</a></li>
    </ul>
</li>