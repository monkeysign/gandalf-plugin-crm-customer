<?php
/**----------------------------------
 * HOOKS PER LA LISTA BACKEND
 * -----------------------------------*/

/**
 * Aggiungo array dei campi da prendere per ogni colonna
 */
hooks()->add_filter( CRM_ADMIN_CUSTOMER_LIST, function ( $record ) {
	$fields = [
		'id',
		[ 'name', 'surname' ],
		'email',
		'id',
		'status'
	];

	return [ $fields, $record ];
}, 10 );

/**
 * Hook per le colonne html della tabella
 */
hooks()->add_filter( CRM_ADMIN_CUSTOMER_LIST_COLUMN, function ( $col = [] ) {
	$columns[0] = '<th><input type="checkbox" class="selectallrow"></th>';
	$columns[1] = '<th>Nome</th>';
	$columns[2] = '<th>Email</th>';
	$columns[3] = '<th class="no-filter">Telefono</th>';
	$columns[4] = '<th class="no-filter">Stato</th>';
	$columns[5] = '<th class="no-filter">Azioni</th>';

	return $columns;
}, 1 );

hooks()->add_filter( CRM_ADMIN_CUSTOMER_LIST_COLUMN, function ( $columns ) {
	foreach ( $columns as $col ) {
		echo $col;
	}
}, 100 );

/**
 * Hook dei filtri per la visualizzazione delle colonne
 */
hooks()->add_filter( CRM_ADMIN_CUSTOMER_LIST_FILTER, function ( $record ) {

	$formatting[0] = function ( $record ) {
		return '<input type="checkbox" class="selectrow" value="' . $record->id . '">';
	};

	$formatting[1] = function ( $record ) {
		return $record->nameSurname;
	};

	$formatting[2] = function ( $record ) {
		return $record->email;
	};

	$formatting[3] = function ( $record ) {
		if ( $record->meta( 'phone' ) ) {
			return $record->meta( 'phone' );
		}
		return '<small>Non presente</small>';
	};

	$formatting[4] = function ( $record ) {
		if ( $record->status == \Ring\Support\Enum\Status::DISABLED ) {
			$status = '<span class="label label-danger">Disabilitato</span>';
		} else if ( $record->status == \Ring\Support\Enum\Status::ENABLED ) {
			$status = '<span class="label label-success">Abilitato</span>';
		}

		return $status;
	};

	$formatting[5] = function ( $record ) {
		return '
<div class="btn-group" role="group">
	<a href="' . path_for( 'admin.crm.customer.update', [ 'id' => $record->id ] ) . '" class="btn btn-sm btn-default">
		<span class="fa fa-pencil"></span>
	</a>
    <a data-path="' . path_for( 'admin.crm.customer.delete', [ 'id' => $record->id ] ) . '" class="btn btn-sm btn-danger delete-item">
        <span class="fa fa-trash-o"></span>
    </a>
</div>
';
	};

	return [ $record, $formatting ];
}, 1 );

/**
 * Effettuo la creazione della tabella applicando i filtri alle colonne
 */
hooks()->add_filter( CRM_ADMIN_CUSTOMER_LIST, function ( $params ) {

	list( $fields, $record ) = $params;

	// do la struttura del dataTable
	$dataTable = new \LiveControl\EloquentDataTable\DataTable( $record->orderByDesc( 'id' ), $fields );

	// Formatto le colonne
	$dataTable->setFormatRowFunction( function ( $record ) {
		list( $record, $formatting ) = hooks()->apply_filters( CRM_ADMIN_CUSTOMER_LIST_FILTER, $record );
		foreach ( $formatting as $format ) {
			$fieldFormat[] = $format( $record );
		}

		return $fieldFormat;
	} );

	// torno l'oggetto
	return $dataTable;
}, 100 );


/**
 * ESEMPIO PER ADDIZIONARE UNA COLONNA ALLA POSIZIONE 3 (DOPO L'EMAIL) CON HOOK
 */
/*hooks()->add_filter( CRM_ADMIN_CUSTOMER_LIST, function ( $params ) {
	list($fields, $record) = $params;
	array_splice( $fields, 3, 0, 'surname' );
	return [ $fields, $record ];
}, 11 );

hooks()->add_filter(CRM_ADMIN_CUSTOMER_LIST_COLUMN, function($columns){
	array_splice( $columns, 3, 0, '<th>Cognome</th>' );
	return $columns;
},2);

hooks()->add_filter( CRM_ADMIN_CUSTOMER_LIST_FILTER, function ( $params ) {
	list($record,$formatting) = $params;
	array_splice( $formatting, 3, 0, function ( $record ) {
		return $record->surname;
	});
	return [$record, $formatting];
},2);*/