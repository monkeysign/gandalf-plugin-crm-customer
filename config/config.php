<?php
return [
	"name"   => 'CRM-Customer',
	"class"  => \Plugins\CRM\Customer\Plugin::class,
	"routes" => __DIR__ . '/../routes/routes.php'
];